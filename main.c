#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp4.h"
#include "utility.h"

#define COUNT 5

// Function to print binary tree in 2D
// It does reverse inorder traversal
void print2DUtil(t_Noeud* root, int space) {
    // Base case
    if (root == NULL)
        return;

    // Increase distance between levels
    space += COUNT;

    // Process right child first
    print2DUtil(root->filsDroite, space);

    // Print current node after space
    // count
    printf("\n");
    for (int i = COUNT; i < space; i++)
        printf(" ");
    printf("%s\n", root->mot);

    // Process left child
    print2DUtil(root->filsGauche, space);
}

// Wrapper over print2DUtil()
void print2D(t_Noeud* root) {
   // Pass initial space count as 0
   print2DUtil(root, 0);
}


void printGivenLevel(t_Noeud* root, int level) {
    if (root == NULL)
        return;
    if (level == 0)
        printf("%s ", root->mot);
    else if (level > 0)
    {
        printf("  [ g ]  "); printGivenLevel(root->filsGauche, level-1);
        printf("  [ d ]  "); printGivenLevel(root->filsDroite, level-1);
    }
}

void printLevelOrder(t_Noeud* root) {
    int h = root->hauteur;
    int i;
    for (i=0; i<=h; i++)
    {
        printGivenLevel(root, i);
        printf("\n");
    }
}


t_Noeud* creer_noeud(char* mot) {
    t_Noeud* newnoeud = (t_Noeud*) malloc(sizeof(t_Noeud));
    strcpy(newnoeud->mot, mot);
    newnoeud->hauteur = 0;

    newnoeud->pere = NULL;
    newnoeud->filsDroite = NULL;
    newnoeud->filsGauche = NULL;
    newnoeud->hauteur = 0;

    return newnoeud;
}

int main(int argc, char *argv[]) {
    FILE* fichier = NULL;
    int caractereActuel = 0;
    fichier = fopen("test.txt", "r");

    if (fichier != NULL)
    {
        // On peut lire et �crire dans le fichier

        // Boucle de lecture des caract�res un � un
        do
        {
            caractereActuel = fgetc(fichier); // On lit le caract�re
            printf("%c", caractereActuel); // On l'affiche
        } while (caractereActuel != EOF); // On continue tant que fgetc n'a pas retourn� EOF (fin de fichier)

        fclose(fichier);
    }
    else
    {
        // On affiche un message d'erreur si on veut
        printf("Impossible d'ouvrir le fichier test.txt");
    }

    t_ListePositions* test_liste  = creer_liste_positions();
    ajouter_position(test_liste, 2, 5, 6);
    ajouter_position(test_liste, 0, 1, 6);
    ajouter_position(test_liste, 0, 2, 6);
    ajouter_position(test_liste, 0, 5, 6);
    ajouter_position(test_liste, 0, 0, 6);
    ajouter_position(test_liste, 1, 4, 6);
    ajouter_position(test_liste, 1, 5, 6);
    ajouter_position(test_liste, 3, 5, 6);

    printf("\n");
    t_Position* temp = test_liste->debut;
    while (temp != NULL) {
        printf("%d, %d, %d\n", temp->numero_ligne, temp->ordre, temp->numero_phrase);
        temp = temp->suivant;
    }


    printf("\n");
    printf("\n");
    printf("\n");
    printf("\n");

    t_Index* pt_index = malloc(sizeof(t_Index));
    pt_index->racine = NULL;
    t_Noeud* n = creer_noeud("d");

    printLevelOrder(n);

    ajouter_noeud(pt_index, n);
    printf("\n------\n");
    ajouter_noeud(pt_index, creer_noeud("h"));
    ajouter_noeud(pt_index, creer_noeud("a"));
    ajouter_noeud(pt_index, creer_noeud("f"));
    ajouter_noeud(pt_index, creer_noeud("q"));
    ajouter_noeud(pt_index, creer_noeud("p"));
    ajouter_noeud(pt_index, creer_noeud("m"));
    ajouter_noeud(pt_index, creer_noeud("c"));
    ajouter_noeud(pt_index, creer_noeud("n"));
    ajouter_noeud(pt_index, creer_noeud("e"));
    ajouter_noeud(pt_index, creer_noeud("r"));
    ajouter_noeud(pt_index, creer_noeud("k"));
    ajouter_noeud(pt_index, creer_noeud("l"));
    ajouter_noeud(pt_index, creer_noeud("u"));
    ajouter_noeud(pt_index, creer_noeud("o"));
    ajouter_noeud(pt_index, creer_noeud("j"));
    printf("\n------\n");
    printf("search : %s ", rechercher_mot(pt_index, "i")->mot);
    printf("\n------\n");
    printf("hauteur racine : %d\n", hauteur(pt_index->racine));
    printf("hauteur D      : %d\n", hauteur(pt_index->racine->filsDroite));
//    printf("hauteur DD     : %d\n", hauteur(pt_index->racine->filsDroite->filsDroite));
//    printf("hauteur G      : %d\n", hauteur(pt_index->racine->filsGauche));
//    printf("hauteur GG     : %d\n", hauteur(pt_index->racine->filsGauche->filsGauche));
//    printf("hauteur GD    : %d\n", hauteur(pt_index->racine->filsGauche->filsDroite));
    print2D(pt_index->racine);

    return 0;
}
