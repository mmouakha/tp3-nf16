#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp4.h"
#include "utility.h"


int max(int x, int y) {
    return (x > y) ? x : y;
}


void strtitle(char* chaine)
{
    if (chaine != NULL) {
        for (char *pointeur = chaine; *pointeur != '\0'; ++pointeur)
        {
            *pointeur = (pointeur == chaine || *(pointeur-1) == ' ') ? toupper(*pointeur) : tolower(*pointeur);
        }
    }

    return;
}


int hauteur(t_Noeud* pt_noeud) {
    if (pt_noeud == NULL) {
        return -1;  // Selon la convention vue en cours, un arbre vide est de hauteur -1
    }
    else if (pt_noeud->filsGauche == NULL && pt_noeud->filsDroite == NULL) {

return 0;
    }
    else if (pt_noeud->filsGauche == NULL) {
        return 1 + pt_noeud->filsDroite->hauteur;
    }
    else if (pt_noeud->filsDroite == NULL) {
        return 1 + pt_noeud->filsGauche->hauteur;
    }
    else {
        return max(1 + pt_noeud->filsGauche->hauteur, 1 + pt_noeud->filsDroite->hauteur);
    }
}

int equilibre(t_Noeud* pt_noeud) {
    if (pt_noeud == NULL) {
        return -1;
    }

    return hauteur(pt_noeud->filsGauche) - hauteur(pt_noeud->filsDroite);
}


t_Noeud* Rotd(t_Noeud* x) {
    if (x->filsGauche != NULL) {
        t_Noeud* y = x->filsGauche;
        t_Noeud* C = y->filsDroite;
        y->pere = x->pere;
        if (x->pere != NULL) {
            if (x->pere->filsGauche == x) {
                x->pere->filsGauche = y;
            }
            else {
                x->pere->filsDroite = y;
            }
        }
        y->filsDroite = x;
        x->pere = y;
        x->filsGauche = C;
        if (C != NULL) {
            C->pere = x;
        }

        // Recupere toutes les hauteurs avant rotation
        int h = x->hauteur;
        int hB = (y->filsGauche != NULL) ? y->filsGauche->hauteur : -1;
        int hC = (C != NULL) ? C->hauteur : -1;
        int hD = (x->filsDroite != NULL) ? x->filsDroite->hauteur : -1;
        int hj = -1;

        // Correction des hauteurs des noeuds du sous-arbre modifie
        x->hauteur = 1 + max(hC, hD);
        y->hauteur = 1 + max(x->hauteur, hB);

        int Delta = y->hauteur - h; // Calcul du Delta
        // Correction hauteur noeuds chemin entre y et racine
        // i est la racine du sous - arbre modifie
        t_Noeud* i = y;
        t_Noeud* p = i->pere;
        t_Noeud* j = NULL;
        while (Delta != 0 && p != NULL) {
            // On utilise la formule du Delta pour avoir une complexite en O(log(n))
            // j racine du sous-arbre non modifie
            j = (p->filsGauche == i) ? j = p->filsDroite : p->filsGauche;
            hj = (j != NULL) ? j->hauteur : -1;
            if (i->hauteur > hj || (i->hauteur && Delta == 1)) {
                p->hauteur = p->hauteur + 1;
                i = p;
                p = p->pere;
            }
            else {
                Delta = 0;
            }
        }

        return y;
    }

    return x;
}

t_Noeud* Rotg(t_Noeud* y) {
    if (y->filsDroite != NULL) {
        t_Noeud* x = y->filsDroite;
        t_Noeud* C = x->filsGauche;
        x->pere = y->pere;
        if (y->pere != NULL) {
            if (y->pere->filsGauche == y) {
                y->pere->filsGauche = x;
            }
            else {
                y->pere->filsDroite = x;
            }
        }
        x->filsGauche = y;
        y->pere = x;
        y->filsDroite = C;
        if (C != NULL) {
            C->pere = y;
        }

        // Recupere toutes les hauteurs avant rotation
        int h = y->hauteur;
        int hB = (y->filsGauche != NULL) ? y->filsGauche->hauteur : -1;
        int hC = (C != NULL) ? C->hauteur : -1;
        int hD = (x->filsDroite != NULL) ? x->filsDroite->hauteur : -1;
        int hj = -1;

        // Correction des hauteurs des noeuds du sous-arbre modifie
        y->hauteur = 1 + max(hB, hC);
        x->hauteur = 1 + max(y->hauteur, hD);

        int Delta = x->hauteur - h; // Calcul du Delta
        // Correction hauteur noeuds chemin entre y et racine
        // i est la racine du sous - arbre modifie
        t_Noeud* i = x;
        t_Noeud* p = i->pere;
        t_Noeud* j = NULL;
        while (Delta != 0 && p != NULL) {
            // On utilise la formule du Delta et non la fonction recursive hauteur(t_Noeud* pt_noeud)
            // pour avoir une complexite en O(log(n)) et non en O(log(n)^2)
            // j racine du sous-arbre non modifie
            j = (p->filsGauche == i) ? j = p->filsDroite : p->filsGauche;
            hj = (j != NULL) ? j->hauteur : -1;
            if (i->hauteur > hj || (i->hauteur && Delta == 1)) {
                p->hauteur = p->hauteur + 1;
                i = p;
                p = p->pere;
            }
            else {
                Delta = 0;
            }
        }

        return x;
    }

    return y;
}
