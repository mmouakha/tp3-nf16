#ifndef H_HEADER_UTILITY_INCLUDED
#define H_HEADER_UTILITY_INCLUDED


int max(int x, int y);
void strtitle(char*);
int hauteur(t_Noeud* pt_noeud);
int equilibre(t_Noeud* pt_noeud);
t_Noeud* Rotd(t_Noeud* x);
t_Noeud* Rotg(t_Noeud* x);


#endif
