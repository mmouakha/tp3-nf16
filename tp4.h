#ifndef H_HEADER_TP4_INCLUDED
#define H_HEADER_TP4_INCLUDED

#define TAILLE_MAX 100

typedef struct t_position t_Position;
struct t_position {
    int numero_ligne;
    int ordre;
    int numero_phrase;
    t_Position* suivant;
};

typedef struct t_listePositions t_ListePositions;
struct t_listePositions {
    t_Position* debut;
    int nb_elements;
};

typedef struct t_noeud t_Noeud;
struct t_noeud {
    char mot[TAILLE_MAX];
    int nb_occurences;
    t_ListePositions positions;
    t_Noeud* filsGauche;
    t_Noeud* filsDroite;

    t_Noeud* pere;
    int hauteur;
};

typedef struct t_index t_Index;
struct t_index {
    t_Noeud* racine;
    int nb_mots_differents;
    int nb_mots_total;
};


t_ListePositions* creer_liste_positions(void);
int ajouter_position(t_ListePositions* listeP, int ligne, int ordre, int num_phrase);
t_Index* creer_index();
t_Noeud* rechercher_mot(t_Index* index, char* mot);
int ajouter_noeud(t_Index* index, t_Noeud* noeud);
int indexer_fichier(t_Index* index, char* filename);


#endif
