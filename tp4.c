#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tp4.h"
#include "utility.h"


t_ListePositions* creer_liste_positions() {
    t_ListePositions* pt_liste_pos = (t_ListePositions*) malloc(sizeof(t_ListePositions));

    if (pt_liste_pos != NULL) {
        pt_liste_pos->debut = NULL;
        pt_liste_pos->nb_elements = 0;
    }

    return pt_liste_pos;
}


int ajouter_position(t_ListePositions* listeP, int ligne, int ordre, int num_phrase) {
    t_Position* pt_new_pos = (t_Position*) malloc(sizeof(t_Position));
    pt_new_pos->numero_ligne = ligne;
    pt_new_pos->ordre = ordre;
    pt_new_pos->numero_phrase = num_phrase;
    pt_new_pos->suivant = NULL;

    t_Position* temp = listeP->debut;

    if (pt_new_pos != NULL) {   // Si l'allocation memoire n'a pas fonctionnee
        if (listeP->debut == NULL || temp->numero_ligne > ligne || (temp->numero_ligne == ligne && temp->ordre > ordre)) {  // Insertion en tete de liste    // Si l'element est a inserer en tete de liste
            pt_new_pos->suivant = temp;
            listeP->debut = pt_new_pos;
        }
        else {  // On parcourt la liste jusqu'a l'emplacement d'insertion
            while (temp->suivant != NULL && temp->suivant->numero_ligne < ligne) {
                temp = temp->suivant;
            }
            if (temp->suivant != NULL && temp->suivant->numero_ligne == ligne) {
                while (temp->suivant != NULL && temp->suivant->ordre < ordre) {
                    temp = temp->suivant;
                }
                if (temp->suivant != NULL && temp->suivant->ordre == ordre) {
                    printf("Position deja existante\n");

                    return 0;
                }
            }
            pt_new_pos->suivant = temp->suivant;
            temp->suivant = pt_new_pos;
        }

        return 1;
    }

    return 0;
}


t_Index* creer_index() {
    t_Index* pt_arbre = (t_Index*) malloc(sizeof(t_Index));

    if (pt_arbre != NULL) {
        pt_arbre->racine = NULL;
        pt_arbre->nb_mots_differents = 0;
        pt_arbre->nb_mots_total = 0;
    }

    return pt_arbre;
}


t_Noeud* rechercher_mot(t_Index* index, char* mot) {
    t_Noeud* temp = index->racine;

    while (temp != NULL && strcmp(temp->mot, mot) != 0) {
        if (strcmp(temp->mot, mot) > 0) {
            temp = temp->filsGauche;
        }
        else if (strcmp(temp->mot, mot) < 0) {
            temp = temp->filsDroite;
        }
    }

    return temp;
}


int ajouter_noeud(t_Index* index, t_Noeud* noeud) {
    t_Noeud* temp = index->racine;
    t_Noeud* pere_temp = NULL;

    if (index->racine == NULL) {
        index->racine = noeud;
    }
    else {
        // Insertion du noeud en O(h) = O(log(n))
        // On ne change JAMAIS la valeur de pere_temp une fois fixee
        while (temp != NULL) {
            if (strcmp(temp->mot, noeud->mot) > 0) {
                pere_temp = temp;
                temp = temp->filsGauche;
            }
            else if (strcmp(temp->mot, noeud->mot) < 0) {
                pere_temp = temp;
                temp = temp->filsDroite;
            }
            else {  // else if (strcmp(temp->mot, noeud->mot) == 0)
                printf("Noeud deja existant");

                return 0;
            }
        }
        if (strcmp(pere_temp->mot, noeud->mot) > 0) {
            pere_temp->filsGauche = noeud;
        }
        else {  // else if (strcmp(pere_temp->mot, noeud->mot) < 0)
            pere_temp->filsDroite = noeud;
        }
        noeud->pere = pere_temp;

        // Mise a jour des hauteurs en O(h) = O(log(n))
        temp = pere_temp;
        int h = 0;
        while (temp != NULL) {
            temp->hauteur = max(temp->hauteur, 1 + h);
            h = temp->hauteur;
            temp = temp->pere;
        }

        // Recherche du premier noeud desiquilibre en O(h) = O(log(n))
        temp = pere_temp;
        while (temp != NULL) {
            temp = temp->pere;
            if (abs(equilibre(temp)) > 1) {
                break;
            }
        }

        // Equilibrage par rotation en O(h) = O(log(n))
        if (equilibre(temp) > 1 && strcmp(temp->filsGauche->mot, noeud->mot) > 0) {     // Gauche-Gauche => Rotd
            if (index->racine == temp) {
                index->racine = Rotd(temp);
            }
            else {
                temp = Rotd(temp);
            }
        }
        else if (equilibre(temp) > 1 && strcmp(temp->filsDroite->mot, noeud->mot) < 0) {   // Gauche-Droite => Rotg + Rotd
            temp->filsGauche = Rotg(temp->filsGauche);
            if (index->racine == temp) {
                index->racine = Rotd(temp);
            }
            else {
                temp = Rotd(temp);
            }
        }
        else if (equilibre(temp) < -1 && strcmp(temp->filsDroite->mot, noeud->mot) < 0) {   // Droite-Droite => Rotg
            if (index->racine == temp) {
                index->racine = Rotg(temp);
            }
            else {
                temp = Rotg(temp);
            }
        }
        else if (equilibre(temp) < -1 && strcmp(temp->filsGauche->mot, noeud->mot) > 0) {   // Droite-Gauche => Rotg + Rotd
            temp->filsDroite = Rotd(temp->filsDroite);
            if (index->racine == temp) {
                index->racine = Rotg(temp);
            }
            else {
                temp = Rotg(temp);
            }
        }
    }

    return 1;
}
